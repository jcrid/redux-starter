var gulp      = require('gulp'),
  babelify    = require('babelify'),
  source      = require('vinyl-source-stream'), //Changes our string from Browserify to a STREAM.
  browserify  = require('browserify'); //Outputs a STRING, though Gulp needs a stream.
  rimraf      = require('gulp-rimraf')
  less        = require('gulp-less')

gulp.task('clean',() => {
    gulp.src('dist/*')
    .pipe(rimraf())
})

gulp.task('browserify', () => {
  browserify('src/app/main.jsx')
    .transform('babelify')
    .bundle()
    .pipe(source('main.js'))
    .pipe(gulp.dest('dist'))
});

gulp.task('copy', () => {
  gulp.src('index.html')
      .pipe(gulp.dest('dist'));
})

gulp.task('less', function () {
    return gulp.src('src/less/main.less')
        .pipe(less('src/less/main.less'))
        .pipe(gulp.dest('dist'));
});

gulp.task('default',['clean','browserify','copy', 'less'], () => {
  return gulp.watch('src/**/*.*', ['browserify'])
});