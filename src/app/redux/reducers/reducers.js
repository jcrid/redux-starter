"use strict";

export const reducer = (state, action) => {
  state = state || [];

  switch(action.type) {
    case 'ADD':
      return [...state, action.person];
    default:
      return state;
  }
};