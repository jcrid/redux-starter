/**
 * Created by jasoncrider on 3/26/16.
 */
import React, {Component} from 'react'

import About from './About.js'

class App extends Component {
    render() {
        const {store} = this.context;

        return (
            <div>
                <h1>Welcome to Nates React-Redux Boilerplate</h1>

                <div>
                    <About />
                </div>
            </div>
        )
    }
}

App.contextTypes = {
    //contextTypes need to be declared for components RECEIVING context
    store: React.PropTypes.object
};

export default App