import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import {store} from './redux/reducers/rootReducer.js'
import App from './components/App.js'

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>, document.getElementById('app'));
};
render();
store.subscribe(render);